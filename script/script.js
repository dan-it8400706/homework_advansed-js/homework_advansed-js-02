const books = [
    { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
    }, 
    {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
    }, 
    { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
    }, 
    {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
    },
    {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
    }
];

//пошук div #root та додавання до нього списку ul

const root = document.getElementById('root')
const ul = document.createElement("ul")
root.insertAdjacentElement('afterbegin', ul)

// вивод масиву до списку

books.forEach(element => {

    try {  
        if (element.author === undefined) {
            throw new Error("Неповні дані: відсутнє поле author"); // (*)
        }
        if (element.name === undefined) {
          throw new Error("Неповні дані: відсутнє поле name"); // (*)
        }    
        if (element.price === undefined) {
            throw new Error("Неповні дані: відсутнє поле price"); // (*)
        }    
        ul.insertAdjacentHTML("beforeend",
        `<li>
        ${element.author}<br>
        ${element.name}<br>
        ${element.price}
        </li><br>`)

    } catch (e) {

        console.log(e.message);
    }
});